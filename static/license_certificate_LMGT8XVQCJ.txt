LICENSE CERTIFICATE: Envato Elements Item
=================================================
This license certificate documents a license to use the item listed below
on a non-exclusive, commercial, worldwide and revokable basis, for
one Single Use for this Registered Project.

Item Title:                      Sora - Minimal Photographer Template 
Item URL:                        https://elements.envato.com/sora-minimal-photographer-template-ZRJ5HZ
Item ID:                         ZRJ5HZ
Author Username:                 mutationthemes
Licensee:                        Sergey Maslennikov
Registered Project Name:         maslenikov.ru
License Date:                    September 6th, 2021
Item License Code:               LMGT8XVQCJ

The license you hold for this item is only valid if you complete your End
Product while your subscription is active. Then the license continues
for the life of the End Product (even if your subscription ends).

For any queries related to this document or license please contact
Envato Support via https://help.elements.envato.com/hc/en-us/requests/new

Envato Elements Pty Ltd (ABN 87 613 824 258)
PO Box 16122, Collins St West, VIC 8007, Australia
==== THIS IS NOT A TAX RECEIPT OR INVOICE ====
